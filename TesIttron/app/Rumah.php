<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rumah extends Model
{
    protected $table = 'rumah';
    protected $fillable = ['id', 'namarumah', 'textsingkat', 'foto', 'harga', 'luas', 'ruangtidur', 'kolamrenang', 'ruangtamu', 'kategori'];
    public $timestamps = false;
}
