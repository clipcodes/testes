<?php

namespace App\Http\Controllers;

use Hash;
use App\Pengguna;
use App\Rumah;
use Illuminate\Http\Request;

class Android extends Controller
{
    function login(Request $req){
        $pengguna = Pengguna::whereemail($req->get('email'))->first();
        if (!empty($pengguna)){
            if (Hash::check($req->get('password'), $pengguna->password)) {
                return response()->json($pengguna);
            } else { 
                return response()->json(['pesan'=>'Password Salah']);
            }
        } else {
            return response()->json(['pesan'=>'Pengguna Tidak Ada']);
        }
    }

    function register(Request $req){
        $pengguna = new Pengguna();
        $pengguna->namalengkap = $req->get('namalengkap');
        $pengguna->email = $req->get('email');
        $pengguna->password = Hash::make($req->get('password'));
        $pengguna->phone = $req->get('phone');
        $pengguna->save();

        return response()->json($pengguna);
    }

    function rumah(){
        $rumah = Rumah::orderby('id', 'DESC')->paginate(6);
        return response()->json($rumah);
    }

    function addrumah(Request $req){
        $rumah = new Rumah();
        $rumah->namarumah = $req->get('namarumah');
        $rumah->textsingkat = $req->get('textsingkat');
        $rumah->foto = $req->get('foto');
        $rumah->harga = $req->get('harga');
        $rumah->luas = $req->get('luas');
        $rumah->ruangtidur = $req->get('ruangtidur');
        $rumah->kolamrenang = $req->get('kolamrenang');
        $rumah->ruangtamu = $req->get('ruangtamu');
        $rumah->kategori = $req->get('kategori');
        $rumah->save();

        return response()->json($rumah);
    }
}
