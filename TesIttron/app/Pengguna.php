<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pengguna extends Model
{
    protected $table = 'pengguna';
    protected $fillable = ['namalengkap', 'email', 'password', 'phone'];
    public $timestamps = false;
    protected $hidden = ['password'];
}
