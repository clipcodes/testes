@extends('master')
@section('content')
<div class="container-fluid mt--6">

      <div class="row">
        <div class="col-lg-8">
          <div class="card-wrapper">
            <!-- Form controls -->

            <!-- HTML5 inputs -->
            <div class="card">
              <!-- Card header -->
              <div class="card-header">
                <h3 class="mb-0">Add New Product</h3>
              </div>
              <!-- Card body -->
              <div class="card-body">
                {{-- <form action="/product/store" method="post" enctype="multipart/form-data">
                  @csrf --}}
                  <form enctype="multipart/form-data">
                  <div class="form-group">
                    <label class="form-control-label" for="title">Product Name</label>
                    <input type="text" class="form-control" id="title" name="title" placeholder="Product Name">
                  </div>
                  
                  <div class="row">
                      <div class="col-sm">
                        <div class="form-group">
                          <label class="form-control-label" for="price">Price</label>
                          <input type="text" class="form-control float" id="price" name="price" placeholder="Price Product">
                        </div>
                      </div>
                      <div class="col-sm">
                        <div class="form-group">
                          <label class="form-control-label" for="exampleFormControlInput1">Discount</label>
                          <input type="text" class="form-control int" id="discount" name="discount" placeholder="Give Discount">
                        </div>
                      </div>
                      <div class="col-sm">
                        <div class="form-group">
                          <label class="form-control-label" for="exampleFormControlInput1">Stock</label>
                          <input type="text" class="form-control int" id="stock" name="stock" placeholder="Stock Of Product">
                        </div>
                      </div>
                  </div>   
 
                  <div class="row">
                    <div class="col-md-6">

                    <div class="form-group">
                      <label class="form-control-label" for="exampleFormControlInput1">Category</label>
                      <select onchange="choosecategory();" id="categorychoose" name="category" class="form-control" data-toggle="select" data-minimum-results-for-search="Infinity">
                        <option>Please Wait..</option>
                      </select>
                    </div>

                    </div>

                    <div class="col-md-6">

                    <div class="form-group">
                      <label class="form-control-label" for="exampleFormControlInput1">Sub Category</label>
                      <select id="subcategorychoose" name="subcategory" class="form-control" data-toggle="select" data-minimum-results-for-search="Infinity">
                        <option>- Choose Category First</option>
                      </select>
                    </div>

                    </div>

                  </div>

                  <div class="form-group">
                    <label class="form-control-label" for="exampleFormControlInput1">Upload Photo Products</label>
                    <div class="custom-file">
                        <input type="file" class="custom-file-input" lang="en" id='files' name="files[]" multiple>
                        <label class="custom-file-label" for="customFileLang">Select file</label>
                    <button type="button" id="clearphoto" class="btn btn-secondary btn-sm float-right m-2">X Clear</button>
                    </div> 
                    <div class="container m-1">
                        <div class="gallery"></div>
                    </div>
                  </div>

                    <div class="form-group">
                      <label class="form-control-label" for="exampleFormControlTextarea1">Description</label>
                      <textarea class="form-control" id="description" name="description" rows="3"></textarea>
                    </div>
 
                <label class="form-control-label mt-3" for="exampleFormControlInput1">Tags</label>
                <div class="form-group">
                  <div class="bootstrap-tagsinput">
                    <input id="tags" type="text" class="form-control" value="Tags, Product" data-toggle="tags" />
                  </div>
                </div>

                <button type="button" id="addproduct" class="btn btn-secondary btn-lg btn-block mt-4">Add Product</button>

                </form>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-4">
          <div class="card-wrapper">
            <!-- Sizes -->

            <!-- Textareas -->
            <div class="card">
              <!-- Card header -->
              <div class="card-header">
                <h3 class="mb-0">Category Manage</h3>
              </div>
              <!-- Card body -->
              <div class="card-body">
                <form>
                  <div class="row">
                    <div class="col-12">
                      <div class="form-group">
                        <label class="form-control-label" for="exampleFormControlInput1">Category</label>
                        <input type="text" class="form-control" id="category" placeholder="Category Name">
                      </div>
                    </div>

                    <div class="col-12">
                      <button type="button" id="addcategory" class="btn btn-m btn-primary btn-block">Add Category</button>
                    </div>

                    <div class="col-12 mt-2">
                      <div id="addstatuscategory"></div>
                    </div>

                </div>
                </form>
              </div>
            </div>
            <!-- Selects -->

            <div class="card">
              <!-- Card header -->
              <div class="card-header">
                <h3 class="mb-0">Sub Category Manage</h3>
              </div>
              <!-- Card body -->
              <div class="card-body">
                <form>
                  <div class="row">
                    <div class="col-12">
                      <div class="form-group">
                        <label class="form-control-label" for="exampleFormControlInput1">Category Parent</label>
                        <select id="categoryparent" class="form-control" data-toggle="select" data-minimum-results-for-search="Infinity">
                          <option value="0">Please wait..</option>
                        </select>
                      </div>
                    </div>
                    <div class="col-12">
                      <div class="form-group">
                        <label class="form-control-label" for="exampleFormControlInput1">Sub Category</label> 
                        <input type="text" class="form-control" id="subcategory" placeholder="Sub Category Name">
                      </div>
                    </div>

                    <div class="col-12">
                      <button type="button" id="addsubcategory" class="btn btn-m btn-primary btn-block">Add Sub Category</button>
                    </div>

                    <div class="col-12 mt-2">
                      <div id="addstatussubcategory"></div>
                    </div>

                </div>
                </form>
              </div>
            </div>
            <!-- File browser -->

            <!-- Checkboxes and radios -->

          </div>
        </div>
      </div>
      <!-- Footer -->
      <footer class="footer pt-0">
        <div class="row align-items-center justify-content-lg-between">
          <div class="col-lg-6">
            <div class="copyright text-center text-lg-left text-muted">
              © 2019 <a href="https://www.creative-tim.com/" class="font-weight-bold ml-1" target="_blank">Creative Tim</a>
            </div>
          </div>
          <div class="col-lg-6">
            <ul class="nav nav-footer justify-content-center justify-content-lg-end">
              <li class="nav-item">
                <a href="https://www.creative-tim.com/" class="nav-link" target="_blank">Creative Tim</a>
              </li>
              <li class="nav-item">
                <a href="https://www.creative-tim.com/presentation" class="nav-link" target="_blank">About Us</a>
              </li>
              <li class="nav-item">
                <a href="http://blog.creative-tim.com/" class="nav-link" target="_blank">Blog</a>
              </li>
              <li class="nav-item">
                <a href="https://www.creative-tim.com/license" class="nav-link" target="_blank">License</a>
              </li>
            </ul>
          </div>
        </div>
      </footer>
    </div>
@endsection

@section('header')
<div class="header bg-primary pb-6">
      <div class="container-fluid">
        <div class="header-body">
          <div class="row align-items-center py-4">
            <div class="col-lg-6 col-7">
              <h6 class="h2 text-white d-inline-block mb-0">Tables</h6>
              <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                  <li class="breadcrumb-item"><a href="#">Tables</a></li>
                </ol>
              </nav>
            </div>
            <div class="col-lg-6 col-5 text-right">
              <a href="#" class="btn btn-sm btn-neutral">New</a>
              <a href="#" class="btn btn-sm btn-neutral">Filters</a>
            </div>
          </div>
        </div>
      </div>
    </div>
@endsection

@section('go')
<script type="text/javascript">
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

$("#addcategory").click(function(e){
    e.preventDefault();
    var category = $("#category").val();
    $.ajax({
       type:'POST',
       url:'/category/store',
       data:{category:category},
       success:function(data){
        var status = '<div class="alert alert-success alert-dismissible fade show" role="alert">'
        +'<span class="alert-icon"><i class="ni ni-like-2"></i></span>'
        +'<span class="alert-text"><strong>Success!</strong> Category Added!</span>'
        +'<button type="button" class="close" data-dismiss="alert" aria-label="Close">'
        +'<span aria-hidden="true">&times;</span>'
        +'</button>'
        +'</div>'

         $('#addstatuscategory').append(status)
         $('#category').val('');

         var category = `<option>- Choose Category</option>`;
         $.each(data, function(key, val) {
             category += `<option value=`+val.id+`>`+val.category+`</option>`;
             console.log(category);
           });
           $("#categorychoose").html(category);
           $("#categoryparent").html(category);
       }
    });
});
 
$("#addsubcategory").click(function(e){
    e.preventDefault();
    var parent = $("#categoryparent").val();
    var subcategory = $("#subcategory").val();
    console.log(parent+subcategory);
    $.ajax({
       type:'POST',
       url:'/subcategory/store',
       data:{parent:parent, subcategory:subcategory},
       success:function(data){
        var status = '<div class="alert alert-success alert-dismissible fade show" role="alert">'
        +'<span class="alert-icon"><i class="ni ni-like-2"></i></span>'
        +'<span class="alert-text"><strong>Success!</strong> Sub Category Added!</span>'
        +'<button type="button" class="close" data-dismiss="alert" aria-label="Close">'
        +'<span aria-hidden="true">&times;</span>'
        +'</button>'
        +'</div>'

        $('#addstatussubcategory').append(status)
        $('#subcategory').val('');

        var subcategory = `<option>- Choose Sub Category</option>`;
        var categorychoose = $("#categorychoose").children("option:selected").val();
        if (parent==categorychoose) {
          console.log(data);

          $.each(data, function(key, val) {
              subcategory += `<option value=`+val.id+`>`+val.subcategory+`</option>`;
              console.log(subcategory);
            });
            $("#subcategorychoose").html(subcategory);
        }
       }
    });
});
     
$(function() {
    // Multiple images preview in browser
    var imagesPreview = function(input, placeToInsertImagePreview) {

        if (input.files) {
            var filesAmount = input.files.length;

            for (i = 0; i < filesAmount; i++) {
                var reader = new FileReader();

                reader.onload = function(event) {
                    $($.parseHTML('<img class="m-1" style="width:30%">')).attr('src', event.target.result).appendTo(placeToInsertImagePreview);
                }

                reader.readAsDataURL(input.files[i]);
            }
        }

    };

    $('#files').on('change', function() {
        $('#clearphoto').show();
        imagesPreview(this, 'div.gallery');
    });
}); 

$('#clearphoto').hide();
$('#clearphoto').click(function(e){ 
  $('#files').val('');  
  $('#clearphoto').hide();
  $('.gallery').empty();
})

$("#addproduct").click(function(e){

   var form_data = new FormData();

   // Read selected files
   var totalfiles = document.getElementById('files').files.length;
   for (var index = 0; index < totalfiles; index++) {
      form_data.append("files[]", document.getElementById('files').files[index]);
   }

   form_data.append('title', $("#title").val());
   form_data.append('price', $("#price").val());
   form_data.append('stock', $("#stock").val());
   form_data.append('discount', $("#discount").val());
   form_data.append('category', $("#categorychoose").val());
   form_data.append('subcategory', $("#subcategorychoose").val());
   form_data.append('tags', $("#tags").val()); 
   form_data.append('description', $("#description").val()); 
  
    $.ajax({
       type:'POST',
       url:'/product/store',
       enctype: 'multipart/form-data',
       cache: false,
       contentType: false,
       processData: false,
       method: 'POST',
       data:form_data,
       success:function(data){
          console.log(data);
       }
    });
});

loadcategory();

function choosecategory() {
 var selectBox = document.getElementById("categorychoose");
 var selectedValue = selectBox.options[selectBox.selectedIndex].value;
 $.getJSON("/subcategoryall/"+selectedValue, function(data){
   console.log(data);
   var subcategory = `<option>- Choose Sub Category</option>`;
   $.each(data, function(key, val) {
       subcategory += `<option value=`+val.id+`>`+val.subcategory+`</option>`;
       console.log(subcategory);
     });
     $("#subcategorychoose").html(subcategory);
   });
}

function loadcategory() {
  $.getJSON("/categoryall", function(data){
    console.log(data);
    var category = `<option>- Choose Category</option>`;
    $.each(data, function(key, val) {
        category += `<option value=`+val.id+`>`+val.category+`</option>`;
        console.log(category);
      });
      $("#categorychoose").html(category);
      $("#categoryparent").html(category);
    });
}
 
$(".int").on("keypress keyup blur",function (event) {    
        $(this).val($(this).val().replace(/[^\d].+/, ""));
            if ((event.which < 48 || event.which > 57)) {
                event.preventDefault();
            }
        });

$(".float").on("keypress keyup blur",function (event) {
      $(this).val($(this).val().replace(/[^0-9\.]/g,''));
              if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
                  event.preventDefault();
              }
          });
</script>
@endsection
