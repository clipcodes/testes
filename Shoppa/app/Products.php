<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Products extends Model
{
    protected $fillable = [
        'title', 'price', 'discount', 'category', 'subcategory', 'description', 'images', 'tags', 'rating', 'viewer', 'stock'
    ];
}
