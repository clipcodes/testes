<?php

namespace App\Http\Controllers;

use App\SubCategory;
use Illuminate\Http\Request;

class SubCategoryController extends Controller
{

    public function subcategoryall($parent)
    {
      $subcategory = SubCategory::whereparent($parent)->get();
      return response()->json($subcategory);
    }

    public function store(Request $request)
    {
        $subcat = new SubCategory();
        $subcat->parent = $request->input("parent");
        $subcat->subcategory = $request->input("subcategory");
        $subcat->save();

        return response()->json(SubCategory::whereparent($request->input("parent"))->get());
    }

}
