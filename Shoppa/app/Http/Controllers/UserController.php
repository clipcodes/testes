<?php

namespace App\Http\Controllers;

use App\User;
use Hash;
use Illuminate\Http\Request;

class UserController extends Controller
{
    function androidlogin(Request $req){
        $req->validate([
            'username' => 'required',
            'password' => 'required',
            'simplevalidation' => 'required'
        ]);

        if ($req->get('simplevalidation') != 'x') {
            return response()->json(['failed'=>'Who are you.']);
        };

        $usr = User::whereusername($req->get('username'))->first();
        if (!empty($usr)){
            if (Hash::check($req->get('password'), $usr->password)) {
                return response()->json($usr);
            } else { 
                return response()->json(['failed'=>'Wrong password.']);
            }
        } else {
            return response()->json(['failed'=>'User "'.$req->get('username').'" is not found.']);
        }
    }

    function androidregister(Request $req){  
        $req->validate([
            'username' => 'required',
            'fullname' => 'required',
            'password' => 'required',
            'phone' => 'required',
            'email' => 'required',
            'simplevalidation' => 'required'
        ]);

        if ($req->get('simplevalidation') != 'x') {
            return response()->json(['failed'=>'Who are you.']);
        };

        $usr = User::whereemail($req->get('email'))->first();
        if (empty($usr)){ 
            $usr = new User();
            $usr->fullname = $req->get('fullname');
            $usr->username = $req->get('username');
            $usr->password = Hash::make($req->get('password'));
            $usr->phone = $req->get('phone');
            $usr->email = $req->get('email'); 
            $usr->save();

            return response()->json($usr);
        } else {
            return response()->json(['failed'=>'Email "'.$req->get('email').'" already exist.']);
        } 
    }  

    function androidregister(Request $req){  
        $req->validate([
            'username' => 'required',
            'fullname' => 'required',
            'password' => 'required',
            'phone' => 'required',
            'email' => 'required',
            'simplevalidation' => 'required'
        ]);

        if ($req->get('simplevalidation') != 'x') {
            return response()->json(['failed'=>'Who are you.']);
        };

        $usr = User::whereemail($req->get('email'))->first();
        if (empty($usr)){ 
            $usr = new User();
            $usr->fullname = $req->get('fullname');
            $usr->username = $req->get('username');
            $usr->password = Hash::make($req->get('password'));
            $usr->phone = $req->get('phone');
            $usr->email = $req->get('email'); 
            $usr->save();

            return response()->json($usr);
        } else {
            return response()->json(['failed'=>'Email "'.$req->get('email').'" already exist.']);
        } 
    }  
}
