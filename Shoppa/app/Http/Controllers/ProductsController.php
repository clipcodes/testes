<?php

namespace App\Http\Controllers;

use App\Products;
use Illuminate\Http\Request;
use Carbon\Carbon;

class ProductsController extends Controller
{
    public function index()
    {
        return view('product.products');
    }

    public function add()
    {
        return view('product.form');
    }

    public function store(Request $request) {
        $images=array();
        $thumb = '';
        
        if ($request->hasfile('files')) {  
            if($files=$request->file('files')) {
                foreach($files as $file) {
                    $name=$file->getClientOriginalName();
                    $uniqid = explode("-", $this->v4())[0];
                    $file->move('product',$uniqid.$name);
                    $images[]=$uniqid.$name;
                }
            }
        } else {
          return $request->get('cok');
        }
        
        $product = new Products();
        $product->title = $request->input("title");
        $product->price = $request->get("price");
        $product->discount = $request->get("discount");
        $product->stock = $request->get("stock");
        $product->category = $request->get("category");
        $product->subcategory = $request->get("subcategory");
        $product->description = $request->get("description");
        $product->images = implode("-", $images); 
        $product->tags = $request->get("tags");
        $product->rating = 0;
        $product->viewer = 0;
        $product->save();
        
        return response()->json($product);
    }

    public function uploadimage(Request $request) { 
        request()->validate([ 
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',

        ]);
    }

    public function show(Products $products)
    {
        //
    }

    public function edit(Products $products)
    {
        //
    }

    public function update(Request $request, Products $products)
    {
        //
    }

    public function destroy(Products $products)
    {
        //
    }


    public static function v4() {
        return sprintf('%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
    
          // 32 bits for "time_low"
          mt_rand(0, 0xffff), mt_rand(0, 0xffff),
    
          // 16 bits for "time_mid"
          mt_rand(0, 0xffff),
    
          // 16 bits for "time_hi_and_version",
          // four most significant bits holds version number 4
          mt_rand(0, 0x0fff) | 0x4000,
    
          // 16 bits, 8 bits for "clk_seq_hi_res",
          // 8 bits for "clk_seq_low",
          // two most significant bits holds zero and one for variant DCE1.1
          mt_rand(0, 0x3fff) | 0x8000,
    
          // 48 bits for "node"
          mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff)
        );
      }
}
