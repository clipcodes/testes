<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('master');
});

Route::get('product', 'ProductsController@index');
Route::get('product/add', 'ProductsController@add');
Route::post('product/store', 'ProductsController@store');

Route::get('categoryall', 'CategoryController@categoryall');
Route::post('category/store', 'CategoryController@store');

Route::get('subcategoryall/{parent}', 'SubCategoryController@subcategoryall');
Route::post('subcategory/store', 'SubCategoryController@store');

Route::post('androidlogin', 'UserController@androidlogin');
Route::post('androidregister', 'UserController@androidregister');
