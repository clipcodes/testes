<?php

use Illuminate\Database\Seeder;

class ProductsExample extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    { 
        //CATEGORY
        DB::table('category')->insert([
            'id' => 1,
            'category' => "Men Fashion",
        ]); 
        DB::table('category')->insert([
            'id' => 2,
            'category' => "Women Clothing",
        ]); 

        //PRODUCTS 
        DB::table('products')->insert([ 
            'title' => "Amazon Essentials Men's Sherpa Lined Full-Zip Hooded Fleece Sweatshirt",
            'price' => "img1.jpg,img2.jpg,img3.jpg,img4.jpg",
            'discount' => "Women Clothing",
            'description' => "Shell: 56% Cotton, 44% Polyester; Lining: 100% Polyester-Imported-Machine Wash-The body and hood of this premium full-zip sweatshirt is lined with plush 100% polyester Sherpa for a casual look that keeps out the cold-Split kangaroo pockets, flat drawstring, and defined seams-Everyday made better: we listen to customer feedback and fine-tune every detail to ensure quality, fit, and comfort-Model is 6'2\" and wearing size Medium",
            'images' => "Women Clothing",
            'tags' => "Women Clothing",
            'categories' => 1,
            'rating' => "Women Clothing",
            'viewer' => "Women Clothing",
            'category' => "Women Clothing",
            'subcategory' => "Women Clothing",
        ]); 
    }
}
